package com.example.helloconstraint

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {
    var mCount: Int = 0
    private lateinit var mShowCount: TextView
    private lateinit var mZeroButton: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mShowCount = findViewById(R.id.show_count)
        mZeroButton = findViewById(R.id.button_zero)
    }

    fun startSecondActivity(view: View) {
        val intent = Intent(this, SecondActivity::class.java)
        val message = mCount.toString()
        intent.putExtra("Count", message)
        startActivity(intent)
    }

    fun countUp(view: View) {
        mCount++
        mShowCount.text = mCount.toString()
        if (mCount != 0){
            mZeroButton.setBackgroundColor(Color.CYAN)
        }
        if (mCount % 2 == 0){
            view.setBackgroundColor(Color.GREEN)
        }
        else {
            view.setBackgroundColor(Color.BLUE)
        }

    }

    fun countZero(view: View) {
        mCount = 0
        mZeroButton.setBackgroundColor(Color.GRAY)
        mShowCount.text = mCount.toString()
    }
}

